package com.training.restone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestoneApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestoneApplication.class, args);
	}

}
