package com.training.restone.vo;

import org.springframework.lang.Nullable;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Null;

public class User {

    @Min(value=1, message = "Enter a valid ID" )
    @Digits(fraction = 0, integer = 10, message ="Enter a valid Id")
    private int id;

    @NotBlank(message = "Name is mandatory")
    private String name;

    @Nullable
    private String address;


    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }
}
