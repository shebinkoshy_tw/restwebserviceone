package com.training.restone.controller;

import com.training.restone.vo.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.concurrent.atomic.AtomicReference;

@RestController
public class WebserviceController {

    @GetMapping("/hello")
    public String hello() {
        return  "hi";
    }


    @PostMapping("/userDetail")
    public @ResponseBody
    Object userDetail(@Valid @RequestBody User user, Errors errors) {
        if (errors.hasErrors()) {
            AtomicReference<String> finalError = new AtomicReference<>("Error: ");
            errors.getAllErrors().forEach((error) -> {
              String errorMessage = error.getDefaultMessage();
              finalError.set(finalError + errorMessage + ". ");
            });
            return new ResponseEntity(finalError.toString().trim(), HttpStatus.BAD_REQUEST);
        }
        return user;
    }

}
